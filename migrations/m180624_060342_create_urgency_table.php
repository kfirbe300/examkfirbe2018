<?php

use yii\db\Migration;

/**
 * Handles the creation of table `urgency`.
 */
class m180624_060342_create_urgency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    
    {
        $this->createTable('urgency', [
            'id' => $this->primaryKey(),
            'name'=> $this->string(),
        ]);

        $this->insert('urgency',array(
    'name'=>'low',
  ));

$this->insert('urgency',array(
    'name'=>'critical',
  ));

$this->insert('urgency',array(
    'name'=>'normal',
  ));


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('urgency');
    }
}
